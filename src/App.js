import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import store from './store';
import SatApp from './SatApp';
import MyAccount from './container/MyAccount';
import Plan from './container/Plan';
import Services from './container/Services';
import Recharge from './container/Recharge';
import Channels from './container/Channels';
import Profile from './container/Profile';

const App = () => {
  return (
    <Provider store={store}>
      <Router>
        <SatApp>
          <Switch>
            <Route path="/my-account" exact component={MyAccount} />
            <Route path="/plan" exact component={Plan} />
            <Route path="/services" exact component={Services} />
            <Route path="/recharge" exact component={Recharge} />
            <Route path="/channels" exact component={Channels} />
            <Route path="/profile" exact component={Profile} />
          </Switch>
        </SatApp>
      </Router>
    </Provider>
  );
}

export default App;