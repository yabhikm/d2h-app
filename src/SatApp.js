import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import { Grid } from '@material-ui/core';
import { useLocation, Redirect } from 'react-router-dom';
import Header from './components/Header';

import { appInit } from './actions';

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
        padding: theme.spacing(2)
    }
}));

const SatApp = ({ user, children, dispatch }) => {
    const classes = useStyles();
    const location = useLocation();
    useEffect(() => {
        dispatch(appInit());
    }, []);

    return location.pathname === '/' ? <Redirect to={`/my-account`} /> : (
        <div className={classes.root}>
            <Grid container spacing={2}>
                <Grid item xs={1}></Grid>
                <Grid item xs={10}>
                    <Grid container spacing={2}>                        
                        <Header user={user}></Header>
                        {children}
                    </Grid>
                </Grid>
                <Grid item xs={1}></Grid>
            </Grid>
        </div>
    );
};

const mapStateToProps = state => ({
    user: state.user || {}
});

export default connect(mapStateToProps)(SatApp);