import user from './user';
import account from './account';
import plans from './plans';
import channels from './channels';
import services from './services';

export default {
    user,
    account,
    plans,
    channels,
    services
};