const channels = (state = [], { type, payload }) => {
    const { channels = [] } = payload || {};
    switch(type) {
        case 'APP_INIT':
            state = [ ...channels ];
        break;
    }
    return state;
};

export default channels;