const account = (state = {}, { type, payload }) => {
    switch(type) {
        case 'APP_INIT':
            state = { ...payload.account };
        break;

        case 'SUBSCRIBE_PLAN':
            state = {
                ...state,
                plan: payload.id,
                balance: state.balance - payload.total,
                channels: payload.channels
            };
        break;

        case 'SUBSCRIBE_SERVICE':
            state = {
                ...state,
                service: payload.id,
                balance: state.balance - payload.price
            };
        break;

        case 'RECHARGE':
            state = {
                ...state,
                balance: state.balance + payload
            };
        break;

        case 'ACTIVATE_CHANNEL':
            state = {
                ...state,
                balance: state.balance - payload.total,
                channels: [...payload.channels]
            };
        break;
    }
    return state;
};

export default account;