const user = (state = {}, { type, payload }) => {
    const { user = {} } = payload || {};
    switch(type) {
        case 'APP_INIT':
            state = { ...user };
        break;

        case 'UPDATE_PROFILE':
            state = {
                ...state,
                email: payload.email,
                mobile: payload.mobile
            };
        break;
    }
    return state;
};

export default user;