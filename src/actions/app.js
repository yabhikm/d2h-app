import { user, plans, channels, account, services } from '../model';

const appInit = () => ({
    type: 'APP_INIT',
    payload: { user, plans, channels, account, services }
});

export default appInit;