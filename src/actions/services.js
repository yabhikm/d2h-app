const subscribeService = payload => ({
    type: 'SUBSCRIBE_SERVICE',
    payload
});

export default subscribeService;