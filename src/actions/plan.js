const subscribePlan = payload => ({
    type: 'SUBSCRIBE_PLAN',
    payload
});

export default subscribePlan;