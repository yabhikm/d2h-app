import { account } from '../model';

export const accountInit = () => ({
    type: 'ACCOUNT_INIT',
    payload: account
});

export const recharge = payload => ({
    type: 'RECHARGE',
    payload
});

export const activateChannel = payload => ({
    type: 'ACTIVATE_CHANNEL',
    payload
});

export const updateProfile = payload => ({
    type: 'UPDATE_PROFILE',
    payload
});