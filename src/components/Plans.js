import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
    Typography,
    Paper,
    Grid,
    Card,
    CardContent,
    ButtonGroup,
    Button,
    CardActions,
    Grow
} from '@material-ui/core';

const useStyles = makeStyles(theme => ({
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    grid: {
        padding: theme.spacing(6)
    },
    actionWrap: {
        flexDirection: 'column',
        paddingBottom: '20px'
    },
    value: {
        textAlign: 'left',
        padding: '5px'
    },
    label: {
        textAlign: 'left',
        padding: '5px'
    }
}));

const Plans = ({ handleSelectPlan, packs, channels }) => {
    const classes = useStyles();
    return (
        <Grow in={true}>
            <Grid container spacing={1} className={classes.grid}>
                {
                    packs.map(plan => (
                        <Grid xs={6} item key={plan.id}>
                            <Card className={classes.root} variant="outlined">
                                <CardContent>
                                    <Typography className={classes.title} color="textSecondary" gutterBottom>
                                        {plan.name}
                                    </Typography>
                                    <Typography variant="h5" component="h2">
                                        {plan.price} Rs / Month.
                                    </Typography>
                                    <small><i>*Channels: {channels.map(ch => (plan.channels.includes(ch.id) && ch.name) || false).filter(i => i).join(', ')}</i></small>
                                </CardContent>
                                <CardActions className={classes.actionWrap}>
                                    <Button variant="contained" color="primary" onClick={() => handleSelectPlan(plan.id)}>Select</Button>
                                </CardActions>
                            </Card>
                        </Grid>
                    ))
                }
            </Grid>
        </Grow>
    );
};

export default Plans;