import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
    Typography,
    Paper,
    Grid,
    Tabs,
    Tab
} from '@material-ui/core';
import { green } from '@material-ui/core/colors';
import { Redirect, useLocation, Link } from 'react-router-dom';

import AccountCircleIcon from '@material-ui/icons/AccountCircle';

const useStyles = makeStyles(theme => ({
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    }
}));

const urls = ['my-account', 'plan', 'services', 'recharge', 'channels'];

const Header = ({ user }) => {
    const classes = useStyles();
    const location = useLocation();
    const [ redirect, setRedirect ] = useState(null);
    const [ activeMenu, setActiveMenu ] = useState(0);
    const handleTabChange = (e, value) => {
        setRedirect(urls[value] ||  0);
    };
    useEffect(() => {
        setActiveMenu(urls.findIndex(i => location.pathname.includes(i)));
        setRedirect(null);
    });
    
    return redirect ? <Redirect to={redirect} /> : (
        <Grid item xs={12}>
            <Paper className={classes.paper}>
                <Grid container>
                    <Grid item xs={1}>
                        <Typography component="h1" variant="h4">
                            Logo
                        </Typography>
                    </Grid>
                    <Grid item xs={10}>
                        <Grid container>
                            <Grid item xs={12}>
                                <Tabs
                                    value={activeMenu < 0 ? 0 : activeMenu}
                                    indicatorColor="primary"
                                    textColor="primary"
                                    onChange={handleTabChange}
                                >
                                    <Tab label="My Account" />
                                    <Tab label="Plan" />
                                    <Tab label="Services" />
                                    <Tab label="Recharge" />
                                    <Tab label="Channels" />
                                </Tabs>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item xs={1} className="account-wrap">
                        <Link to="/profile">
                            <AccountCircleIcon style={{ fontSize: 40, color: green[500] }}></AccountCircleIcon>
                            { user.name }
                        </Link>
                    </Grid>
                </Grid>
            </Paper>
        </Grid>
    );
};

export default Header;