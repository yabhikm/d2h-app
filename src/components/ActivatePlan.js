import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
    Grid,
    Card,
    CardContent,
    ButtonGroup,
    Button,
    CardActions,
    Grow
} from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';

const useStyles = makeStyles(theme => ({
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    grid: {
        padding: theme.spacing(6)
    },
    actionWrap: {
        flexDirection: 'column',
        paddingBottom: '20px'
    },
    value: {
        textAlign: 'left',
        padding: '5px'
    },
    label: {
        textAlign: 'left',
        padding: '5px'
    }
}));

const ActivatePlan = ({ setStep, plan, account, onPlanSubscribe }) => {
    const classes = useStyles();
    const [ months, setMonths ] = useState(1);
    const [ total, setTotal ] = useState(plan.price);
    const [ discount, setDiscount ] = useState(0);
    const [ alert, setAlert ] = useState({ severity: '', msg: '' });
    const [ disable, setDisable ] = useState(false);
    const handleIncrement = () => {
        const m = months + 1 > 12 ? months : months + 1;
        setMonths(m);
        calculateAmount(m);
    };
    const handleDecrement = () => {
        const m = months - 1 < 1 ? months : months - 1;
        setMonths(m);
        calculateAmount(m);
    };
    const handleCancel = () => {
        setStep(1);
    };
    const calculateAmount = (m) => {
        let t = m * plan.price;
        const d = m > 2 ? (10 / 100 ) * t : 0;
        t -= d;
        setTotal(t);
        setDiscount(d);
    };
    const handlePayment = () => {
        if (total > account.balance) {
            setAlert({severity: 'error', msg: 'Insufficient balance'});
            setTimeout(() => {
                setAlert({severity: '', msg: ''});
            }, 5000);
            return;
        };
        onPlanSubscribe(plan.id, total);
        setDisable(true);
        setAlert({severity: 'success', msg: 'Subscibed to plan successfully'});
    };
    return (
        <Grow in={true}>
            <Grid container spacing={1} className={classes.grid}>
                <Grid xs={3} item></Grid>
                <Grid xs={6} item>
                    {alert.msg && <Alert severity={alert.severity}>{alert.msg}</Alert>}
                    <Card className={classes.root} variant="outlined">
                        <CardContent>
                            <Grid container>
                                <Grid item xs={8} className={classes.label}><b>Plan:</b></Grid>
                                <Grid item xs={4} className={classes.value}>{plan.name}</Grid>
                            </Grid>

                            <Grid container>
                                <Grid item xs={8} className={classes.label}><b>Price:</b></Grid>
                                <Grid item xs={4} className={classes.value}>{plan.price}</Grid>
                            </Grid>

                            <Grid container>
                                <Grid item xs={8} className={classes.label}>
                                    <b>Months:</b>(<i>*10% off on recharge for 3 months or more</i>)
                                    <small></small>
                                </Grid>
                                <Grid item xs={4} className={classes.value}>
                                    <ButtonGroup size="small" aria-label="small outlined button group">
                                        <Button onClick={() => handleDecrement()}>-</Button>
                                        {months && <Button disabled>{months}</Button>}
                                        <Button onClick={() => handleIncrement()}>+</Button>                                        
                                    </ButtonGroup>
                                </Grid>
                            </Grid>
                            <Grid container>
                                <Grid item xs={8} className={classes.label}><b>Discount:</b></Grid>
                                <Grid item xs={4} className={classes.value}>{discount}</Grid>
                            </Grid>

                            <Grid container>
                                <Grid item xs={8} className={classes.label}><b>Total:</b></Grid>
                                <Grid item xs={4} className={classes.value}>{total}</Grid>
                            </Grid>
                        </CardContent>
                        <CardActions className={classes.actionWrap}>
                            <Grid container>
                                <Grid item xs={6}>
                                    <Button variant="contained" color="default" onClick={handleCancel}>Back</Button>
                                </Grid>

                                <Grid item xs={6}>
                                    <Button variant="contained" color="primary" disabled={disable} onClick={handlePayment}>Make Payment</Button>
                                </Grid>
                            </Grid>
                        </CardActions>
                    </Card>
                </Grid>
            </Grid>
        </Grow>
    );
};

export default ActivatePlan;