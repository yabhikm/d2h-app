import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import {
    Typography,
    Paper,
    Grid,
    Card,
    CardContent,
    Button,
    CardActions,
    Grow,
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
    Checkbox
} from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';

import { activateChannel } from '../actions';

const useStyles = makeStyles(theme => ({
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    grid: {
        padding: theme.spacing(6)
    },
    actionWrap: {
        flexDirection: 'column',
        paddingBottom: '20px'
    },
    label: {
        textAlign: 'left',
        paddingLeft: '15px'
    }
}));

const Channels = ({ dispatch, allChannels, account }) => {
    const classes = useStyles();
    const [ alert, setAlert ] = useState({ severity: '', msg: '' });
    const [ selectedChannels, setSelectedChannels ] = useState([]);
    const [ total, setTotal ] = useState(0);
    const handlePayment = () => {
        if (!selectedChannels.length) return;

        if (total > account.balance) {
            setAlert({severity: 'error', msg: 'Insufficient balance'});
            return;
        }

        dispatch(activateChannel({channels: selectedChannels, total}));
        setAlert({severity: 'success', msg: 'Channels activated successfully'});
    };
    const handleToggle = id => {
        if (account.channels.includes(id)) return null;
        const index = selectedChannels.indexOf(id);
        const newChannels = [...selectedChannels];
        if (index === -1) {
            newChannels.push(id);
        } else {
            newChannels.splice(index, 1);
        }
        setSelectedChannels(newChannels);
        let amount = 0;
        allChannels.map(i => {
            if(newChannels.includes(i.id)) amount += i.price;
        });
        setTotal(amount);
    };

    return (
        <Grid item xs={12}>
            <Paper className={classes.paper}>
                <Grid container>
                    <Grid item xs={12}>
                        <Typography component="h1" variant="h4">
                            Channels
                        </Typography>
                    </Grid>
                </Grid>
                
                <Grid container spacing={1} className={classes.grid}>
                    {alert.msg && <Grid xs={12} item><Alert severity={alert.severity}>{alert.msg}</Alert></Grid>}
                    <Grid xs={3} item></Grid>
                    <Grid xs={6} item>
                        <Grow in={true}>
                            <Card className={classes.root} variant="outlined">
                                <CardContent>
                                    <List className={classes.root}>
                                        {allChannels.map(value => {
                                            const labelId = `checkbox-list-label-${value.id}`;

                                            return (
                                            <ListItem key={value.id} role={undefined} dense button onClick={() => handleToggle(value.id)}>
                                                <ListItemIcon>
                                                    <Checkbox
                                                        edge="start"
                                                        checked={account.channels.includes(value.id) || selectedChannels.includes(value.id)}
                                                        tabIndex={-1}
                                                        disableRipple
                                                        disabled={account.channels.includes(value.id)}
                                                        inputProps={{ 'aria-labelledby': labelId }}
                                                    />
                                                </ListItemIcon>
                                                <ListItemText id={labelId} primary={`${value.name} - ${value.price} Rs.`} />
                                            </ListItem>
                                            );
                                        })}
                                    </List>
                                </CardContent>
                                <Typography component="h1" variant="h6" className={classes.label}>
                                    <b>Total:</b> {total}
                                </Typography>
                                <CardActions className={classes.actionWrap}>
                                    <Button variant="contained" color="primary" disabled={alert.severity === 'success'} onClick={handlePayment}>Make Payment</Button>
                                </CardActions>
                            </Card>
                        </Grow>
                    </Grid>
                </Grid>
            </Paper>
        </Grid>
    );
};

const mapStateToProps = (state) => ({
    account: state.account || {},
    allChannels: state.channels || []
});
export default connect(mapStateToProps)(Channels)