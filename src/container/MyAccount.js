import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import {
    Typography,
    Paper,
    Grid,
    Card,
    CardContent,
    Button,
    CardActions,
    Grow
} from '@material-ui/core';

const useStyles = makeStyles(theme => ({
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    grid: {
        padding: theme.spacing(3)
    },
    actionWrap: {
        flexDirection: 'column',
        paddingBottom: '20px'
    }
}));

const MyAccount = ({ account, plans, services }) => {
    const classes = useStyles();
    const plan = (account.plan && plans.filter(i => i.id === account.plan)[0]) || {};
    const service = (account.service && services.filter(i => i.id === account.service)[0]) || {};
    return (
        <Grid item xs={12}>
            <Paper className={classes.paper}>
                <Grid container>
                    <Grid item xs={12}>
                        <Typography component="h1" variant="h4">
                            My Account
                        </Typography>
                    </Grid>
                </Grid>
                <Grid container spacing={1} className={classes.grid}>
                    <Grid xs={4} item>
                        <Grow in={true} timeout={500}>
                            <Card className={classes.root} variant="outlined">
                                <CardContent>
                                    <Typography className={classes.title} color="textSecondary" gutterBottom>
                                        Account Balance
                                    </Typography>
                                    <Typography variant="h5" component="h2">
                                        {account.balance} Rs.
                                    </Typography>
                                </CardContent>
                                <CardActions className={classes.actionWrap}>
                                    <Link  to="/recharge">
                                        <Button variant="contained" color="primary">Recharge</Button>
                                    </Link>
                                </CardActions>
                            </Card>
                        </Grow>
                    </Grid>

                    <Grid xs={4} item>
                        <Grow in={true} timeout={500}>
                            <Card className={classes.root} variant="outlined">
                                <CardContent>
                                    <Typography className={classes.title} color="textSecondary" gutterBottom>
                                        Plan
                                    </Typography>
                                    <Typography variant="h5" component="h2">
                                        {plan.name ? plan.name.toUpperCase() : 'No Plan Subscribed'}
                                    </Typography>
                                </CardContent>
                                <CardActions className={classes.actionWrap}>
                                    <Link  to="/plan">
                                        <Button variant="contained" color="primary">View Plans</Button>
                                    </Link>
                                </CardActions>
                            </Card>
                        </Grow>
                    </Grid>

                    <Grid xs={4} item>
                        <Grow in={true} timeout={500}>
                            <Card className={classes.root} variant="outlined">
                                <CardContent>
                                    <Typography className={classes.title} color="textSecondary" gutterBottom>
                                        Service
                                    </Typography>
                                    <Typography variant="h5" component="h2">
                                    {service.name ? service.name.toUpperCase() : 'No Service Subscribed'}
                                    </Typography>
                                </CardContent>
                                <CardActions className={classes.actionWrap}>
                                    <Link  to="/services">
                                        <Button variant="contained" color="primary">Subscribe</Button>
                                    </Link>
                                </CardActions>
                            </Card>
                        </Grow>
                    </Grid>
                </Grid>
            </Paper>
        </Grid>
    );
};

const mapStateTOProps = state => ({
    account: state.account || {},
    plans: state.plans || [],
    services: state.services || [],
});

export default connect(mapStateTOProps)(MyAccount);