import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import {
    Typography,
    Paper,
    Grid,
    Card,
    CardContent,
    Button,
    CardActions,
    Grow,
    FormControl,
    InputLabel,
    Input
} from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';

import { recharge } from '../actions';

const useStyles = makeStyles(theme => ({
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    grid: {
        padding: theme.spacing(6)
    },
    actionWrap: {
        flexDirection: 'column',
        paddingBottom: '20px'
    },
    value: {
        textAlign: 'left',
        padding: '5px'
    },
    label: {
        textAlign: 'left',
        padding: '5px'
    }
}));

const Recharge = ({ dispatch }) => {
    const classes = useStyles();
    const [ alert, setAlert ] = useState({ severity: '', msg: '' });
    const [ amount, setAmount ] = useState(0);
    const handleChange = e => {
        console.log(e.target.value);
        setAmount(e.target.value);
    };
    const handlePayment = () => {
        dispatch(recharge(+amount));
        setAlert({severity: 'success', msg: 'Recharge successfully'});
    };
    
    return (
        <Grid item xs={12}>
            <Paper className={classes.paper}>
                <Grid container>
                    <Grid item xs={12}>
                        <Typography component="h1" variant="h4">
                            Recharge
                        </Typography>
                    </Grid>
                </Grid>
                
                <Grid container spacing={1} className={classes.grid}>
                    {alert.msg && <Grid xs={12} item><Alert severity={alert.severity}>{alert.msg}</Alert></Grid>}
                    <Grid xs={3} item></Grid>
                    <Grid xs={6} item>
                        <Grow in={true}>
                            <Card className={classes.root} variant="outlined">
                                <CardContent>
                                    <FormControl>
                                        <InputLabel htmlFor="amount">Amount</InputLabel>
                                        <Input type="number" id="amount" value={amount} onChange={handleChange} />
                                    </FormControl>
                                </CardContent>
                                <CardActions className={classes.actionWrap}>
                                    <Button variant="contained" color="primary" disabled={alert.msg} onClick={() => handlePayment()}>Make Payment</Button>
                                </CardActions>
                            </Card>
                        </Grow>
                    </Grid>
                </Grid>
            </Paper>
        </Grid>
    );
};

export default connect()(Recharge);