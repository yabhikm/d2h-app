import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import {
    Typography,
    Paper,
    Grid,
    Card,
    CardContent,
    Button,
    CardActions,
    Grow,
    Input
} from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';

import { updateProfile } from '../actions';

const useStyles = makeStyles(theme => ({
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    grid: {
        padding: theme.spacing(6)
    },
    actionWrap: {
        flexDirection: 'column',
        paddingBottom: '20px'
    },
    label: {
        textAlign: 'left',
        paddingLeft: '15px'
    }
}));

const Profile = ({ dispatch, user }) => {
    const classes = useStyles();
    const [ alert, setAlert ] = useState({ severity: '', msg: '' });
    const [ email, setEmail ] = useState('');
    const [ mobile, setMobile ] = useState('');
    useEffect(() => {
        if (user.mobile) {
            setEmail(user.email);
            setMobile(user.mobile);
        }
    }, [user]);
    const handleSubmit = () => {
        setAlert({ severity: 'success', msg: 'Profile updated successfull' });
        dispatch(updateProfile({mobile, email}));
    };
    const handleChange = (e, field) => {
        switch(field) {
            case 'email':
                setEmail(e.target.value);
            break;

            case 'mobile':
                setMobile(e.target.value);
            break;
        }
    };

    return (
        <Grid item xs={12}>
            <Paper className={classes.paper}>
                <Grid container>
                    <Grid item xs={12}>
                        <Typography component="h1" variant="h4">
                            Profile
                        </Typography>
                    </Grid>
                </Grid>
                
                <Grid container spacing={1} className={classes.grid}>
                    {alert.msg && <Grid xs={12} item><Alert severity={alert.severity}>{alert.msg}</Alert></Grid>}
                    <Grid xs={3} item></Grid>
                    <Grid xs={6} item>
                        <Grow in={true}>
                            <Card className={classes.root} variant="outlined">
                                <CardContent>
                                    <Grid container className={classes.grid}>
                                        <Grid item xs={12}>
                                            <Input inputProps={{ 'aria-label': 'description' }} value={email} onChange={e => handleChange(e, 'email')} />
                                        </Grid>
                                    </Grid>
                                    <Grid container>
                                        <Grid item xs={12}>
                                            <Input inputProps={{ 'aria-label': 'description' }} value={mobile} onChange={e => handleChange(e, 'mobile')} />
                                        </Grid>
                                    </Grid>
                                </CardContent>
                                <CardActions className={classes.actionWrap}>
                                    <Button variant="contained" color="primary" disabled={alert.severity === 'success'} onClick={handleSubmit}>Submit</Button>
                                </CardActions>
                            </Card>
                        </Grow>
                    </Grid>
                </Grid>
            </Paper>
        </Grid>
    );
};

const mapStateToProps = (state) => ({
    user: state.user || {}
});
export default connect(mapStateToProps)(Profile)