import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import {
    Typography,
    Paper,
    Grid
} from '@material-ui/core';

import { subscribePlan } from '../actions';

import Plans from '../components/Plans';
import ActivatePlan from '../components/ActivatePlan';

const useStyles = makeStyles(theme => ({
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    grid: {
        padding: theme.spacing(6)
    },
    actionWrap: {
        flexDirection: 'column',
        paddingBottom: '20px'
    },
    value: {
        textAlign: 'left',
        padding: '5px'
    },
    label: {
        textAlign: 'left',
        padding: '5px'
    }
}));

const renderStep = ({ step, handleSelectPlan, setStep, packs, channels, selectedPlan, account, onPlanSubscribe }) => {
    switch (step) {
        case 1:
            return <Plans
                handleSelectPlan={handleSelectPlan}
                packs={packs}
                channels={channels}
            ></Plans>

        case 2:
            return <ActivatePlan
                setStep={setStep}
                plan={selectedPlan}
                account={account}
                onPlanSubscribe={onPlanSubscribe}
            ></ActivatePlan>
    };
};

const Plan = ({ dispatch, packs, channels, account }) => {
    const classes = useStyles();
    const [ step, setStep ] = useState(1);
    const [ selectedPlan, setSelectedPlan ] = useState({});
    const handleSelectPlan = pid => {
        setSelectedPlan(packs.filter(pack => pack.id === pid)[0] || {});
        setStep(2);
    };
    const onPlanSubscribe = (id, total) => {
        total <= account.balance && dispatch(subscribePlan({ id, total, channels: packs.filter(i => i.id === id)[0].channels }));
    };
    return (
        <Grid item xs={12}>
            <Paper className={classes.paper}>
                <Grid container>
                    <Grid item xs={12}>
                        <Typography component="h1" variant="h4">
                            { step === 1 ? 'Plan' : 'Activate Plan' }
                        </Typography>
                    </Grid>
                </Grid>
                {renderStep({
                    step,
                    handleSelectPlan,
                    setStep,
                    packs,
                    channels,
                    selectedPlan,
                    account,
                    onPlanSubscribe
                })}
            </Paper>
        </Grid>
    );
};

const mapStateToProps = (state) => {
    return {
        packs: state.plans || [],
        channels: state.channels || [],
        account: state.account || {}
    }
};
export default connect(mapStateToProps)(Plan);