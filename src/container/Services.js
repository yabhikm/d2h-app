import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import {
    Typography,
    Paper,
    Grid,
    Card,
    CardContent,
    Button,
    CardActions,
    Grow
} from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';

import { subscribeService } from '../actions';

const useStyles = makeStyles(theme => ({
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    grid: {
        padding: theme.spacing(6)
    },
    actionWrap: {
        flexDirection: 'column',
        paddingBottom: '20px'
    },
    value: {
        textAlign: 'left',
        padding: '5px'
    },
    label: {
        textAlign: 'left',
        padding: '5px'
    }
}));

const Services = ({ dispatch, allServices, account }) => {
    const classes = useStyles();
    const [ alert, setAlert ] = useState({ severity: '', msg: '' });
    const handlePayment = id => {
        const { id: sid, price } = allServices.filter(i => i.id === id)[0] || {};
        if (account.balance < price) {
            setAlert({severity: 'error', msg: 'Insufficient balance'});
            return;
        }
        dispatch(subscribeService({ id: sid, price })); 
        setAlert({severity: 'success', msg: 'Subscibed to service successfully'});
    };
    
    return (
        <Grid item xs={12}>
            <Paper className={classes.paper}>
                <Grid container>
                    <Grid item xs={12}>
                        <Typography component="h1" variant="h4">
                            Services
                        </Typography>
                    </Grid>
                </Grid>
                
                <Grid container spacing={1} className={classes.grid}>
                    {alert.msg && <Grid xs={12} item><Alert severity={alert.severity}>{alert.msg}</Alert></Grid>}
                    {
                        allServices.map(s => (
                            <Grid xs={6} item key={s.id}>
                                <Grow in={true}>
                                    <Card className={classes.root} variant="outlined">
                                        <CardContent>
                                            <Typography className={classes.title} color="textSecondary" gutterBottom>
                                                {s.name}
                                            </Typography>
                                            <Typography variant="h5" component="h2">
                                                {s.price} Rs.
                                            </Typography>
                                        </CardContent>
                                        <CardActions className={classes.actionWrap}>
                                            <Button variant="contained" color="primary" disabled={account.service === s.id} onClick={() => handlePayment(s.id)}>Make Payment</Button>
                                        </CardActions>
                                    </Card>
                                </Grow>
                            </Grid>
                        ))
                    }
                </Grid>
            </Paper>
        </Grid>
    );
};

const mapStateToPRops = (state) => ({
    allServices: state.services || [],
    account: state.account || {}
});

export default connect(mapStateToPRops)(Services);