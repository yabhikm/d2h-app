const user = {
    name: 'Abhishek',
    email: 'yabhishekkm@gmail.com',
    mobile: '9823932282'
};

const account = {
    balance: 100,
    plan: null,
    service: null,
    channels: []
};

const plans = [
    {
        id: 1,
        name: 'Gold',
        price: 100,
        channels: [1, 2, 3, 4, 5]
    },
    {
        id: 2,
        name: 'Silver',
        price: 50,
        channels: [1, 2, 3]
    }
];

const channels = [
    { id: 1, name: 'Zee', price: 10 },
    { id: 2, name: 'Sony', price: 15 },
    { id: 3, name: 'Star Plus', price: 20 },
    { id: 4, name: 'Discovery', price: 10 },
    { id: 5, name: 'NatGeo', price: 20 }
];

const services = [
    { id: 1, name: 'Learn English', price: 200 },
    { id: 2, name: 'Learn Cooking', price: 100 }
];

export {
    user,
    account,
    plans,
    channels,
    services
};